# Dilemma between stability and maneuverability

We all know the wave function would collapse when it is measured. So the decoherence of qubits is equivalent to be stared by an **All Seeing Eye**, the environment.

But all operations of qubits require coupling between qubits and classical field. For example, a superconducting qubit capacitively couple with a resonator, which is typically a CPW wire, in order to non-demolition measurement. Weak coupling results long measuring time cost that inducing more error on data qubits in surface code; however strong coupling results high decoherence rate that inducing more error during gate operations.

![All Seeing Eyes](/docs/assets/img/ase.jpg "All Seeing Eyes")

# Is there any optimum strategy?

You could say there is always a optimized parameter set if we know the constrains. But the sweet point strongly depends on the **numerical quantized capability** of the technology stack. Researchers now seems use greedy method, sadly most of them from extremely local perspective, to build the entire quantum computer. Since it is convinient considering a strategy from the most abstract and macroscopical or the most specific and microscopical view.

As a consequence, we are ***burning the canddle at the both ends !***
