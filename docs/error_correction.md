# The linchpin is the error correction code

## Regard error correction code as a seed

We find error correction takes center position of the decomposition chain. It is also a mysterious field with charming theory in algibra and physcis perspective. When a error correction code is chosen, one could determine best logical instruciton set and best physical realization in a much easier way.

Taking classical surface code as an example, to macroscopic region, it gives a well extrapolated estimation of qubits and time cost for a certain algorithmm, such as Shor's algorithm. Nonetheless, the scale of at least **$10^3$ to $10^4$ logic qubits** using over **10 months** must be kept a secret to the investors.

And the code also gives a always runing repeating unit that occupies most run time of the processor. It require the processor to run not only low-error-rate Hadamard gate and CNOT or CZ gate but also measurement and reset commands. The topology can be any 2D tiled structure but a checkerboard connection seems to be superior since the **state-of-art 99% measurement fidelity** still induce more error as the qubit connection getting sparse. These requirements led to the design of Google's Sycamore and its support system.

Anyway this example gives us a panoramic view of how the seed(a intensively studied code) grows the root(the QPU and its support system), the trunk(the algorithm) and the fruitage(the applications).

When seeding another code, such as Floquet code, it would grow into a different tree.

I call it a ***quantum computing coenosium.***

![oak](/docs/assets/img/oak.jpg "oak")

## Cultivation and hybridization

In agriculture people obtain good species by cultivation and hybridization. We could also find truely industrial and commercial quantum computing system in those way.

1. Achieving ultra low infomation loss by improving at-ground level dynamics in the QPU.
2. Getting best performance by combining genetically similar system to exert their advantages.

## A step beyond NISQ

John Preskill discribe recent QPU developing statement as NISQ(Noisy Intermediate-scale Quantum){cite:p}`Preskill2018quantumcomputingin`. Yet in such statement quantum techonology showes non competitive capability with classical machines. Let us assume what we can do at the midway towards the totally error correted era.

1. **Basic problem study related with better error correction.** Despite Clifford gate based error correction circuit can be efficiently simulated by classical computor, the actual errors accuring on device with mixed and biased decoherent and coherent are difficult modeled by simple assuption. So it is important to use a QPU with **$10^3$** qubits and **$10^{-3}$** average gate error rate to learn practical error model. Fantasic quantum infomation and condense matter theory that may give illumination would also be test on this kind of QPUs.

2. **Measurement enhanced quantum compution and first logic qubit.** In roughly numerical simulation, it is possible to build a single logic qubit with **$10^{-8}$** logic error on a QPU having **$10^4$** qubits and **$10^{-3}$** average gate error rate. Any of 3 target could be slightly relaxed. On these QPUs, are there any measurement enhanced quantum compution could excuted? These partial and sparse error correction strategies help some algorithm like QAOA to achieve better performance than NISQ devices.

3. **Fault tolerent algorithm on smaller scale error corrected device.** Except for the Shor's algorithm, some algorithm using less resource may make quantum computing truely worthwhile when a QPU with **$10^{-10}$** logic error and $10^2$ logic qubits is fabricated. Comparing with poor NISQ devices or tardy classical computer, simulate many body system with it would get clear and convidenced physical results.

To be honest, We still do not know such **a step** beyound NISQ is **a vacation tour or an interstellar navigation.**

```{bibliography}
:filter: docname in docnames
```
