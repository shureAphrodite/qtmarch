# Optimization at different level

Optimization is really important especially at physical quantum gate level. An one-tenth qubit resource required system needs almost one percent energy consumption by cryogenic system and electronic system. As cryogenic system could not scale up arbitrarily, the qubit resource requirement would decide a quantum computor to be a gigantic monster or a cute kitty.

## Error correction code with biased error

The prototype of surface code is based on two types of interval stablizers. There are other different codes called Clifford-deformed surface codes.These codes perform better than traditional one when assuming the errors are biased to Pauli-Z direction.
