# Quantum computing is absolutely an art with wave function

## Best introduction that as short as a mini-skirt

This online document will give an exhaustive and thorough introduction of quantum computing architecture from the perspective of an quantum engineer who is specialized in the field of superconducting platform. I mean trying my best.

First, for layman to this field, I provide links of two prestigious tech-giants, [Google](https://quantumai.google) and [IBM](https://www.ibm.com/topics/quantum-computing), which are at the forefront of building a superconducting quantum computer. And obviously, for professional readers, the progress and achievement of these two companies would be known and followed almost at once. They represent, in a way, two kinds of technology philosophy belongs to academia and industry, respectively. One is exploring wide range open questions in quantum computing field with a competent system while another is expanding amount of the qubits and constructing customer environment as soon as possible.

I do not want to waste time on bragging how amazing the quantum computing is. On the contrary, I aim revealing the technology gaps and potential solutions at different level. Any nowadays claimed commercial application is not credible, just like the Mechanical Turk.

## Peeling the onion --- from black box algorithm to first-principle mechanism

Here I give a anatomy to describe how fundamental mechanism forms an algorithm.
![quantum computing onion](/docs/assets/img/onion.jpg "quantum computing onion")

But the content of this illustration still needs detailed explaination to avoid misunderstandings.

1. The example of quantum algorithm in the illustation indicating machine learning. But unfortunately whether this algorithm has promising advantage in quantum is still the unkown. I use a most unsuitable example here just because it looks coooool！**OvO**

2. The operations on logic qubits is totally different from what act on physical qubits. And they would vary with encoding strategies, like stablizer code, topological anyons and so on. I call them ***packaged quantum circuit.***

3. I cite a figure in a *Nature* article {cite:p}`acharya_suppressing_2023`.  An actual logical qubit may need over 1000 physical qubits rather than 100 in the illustration. As for most of nowadays quantum computation demonstations, we regard a physics qubit as a logic qubit since error correction is about to touching the break-even point yet.

4. I take **Xmon-Xmon-Xmon** frame as a example{cite:p}`ykps_tunable_2018`. There are many combination scheme using varies type of differential or single-end flux qubit. And whether the qubits' frequency are fixed determines the two-qubit gates or multi-qubit gates are applied by DC or microwave control pulse.

```{bibliography}
:filter: docname in docnames
```
